package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    
    public ModelDuck() {
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new Quack());
    }
    
    @Override
    public void display() {
        // TODO Auto-generated method stub
        System.out.println("I'm a mallard duck!");
    }
}
