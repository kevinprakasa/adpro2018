package sorting;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;
import static sorting.Main.convertInputFileToArray;

public class SortAndSearchTest {
    int[] sequenceInput1 = convertInputFileToArray();

    public SortAndSearchTest() throws IOException {
    }

    @Test
    public void testSorter() {
        int[] slowSorted = Sorter.slowSort(sequenceInput1);
        int[] fastSorted = Sorter.quickSort(sequenceInput1);
        assertTrue(Arrays.equals(slowSorted, fastSorted));
    }

    @Test
    public void testFinder() {
        int slowFinder = Finder.slowSearch(sequenceInput1, 6112);
        int fastFinder = Finder.fastSearch(sequenceInput1, 6112);
        assertTrue(slowFinder == fastFinder);
    }

}
