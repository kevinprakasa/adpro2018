package sorting;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    private static String pathFile = "../plainTextDirectory/input/sortingProblem.txt";
    private static int numberOfItemToBeSorted = 50000;

    public static void main(String[] args) throws IOException {
        System.out.println("///Using fast Search and sorting");

        int[] sequenceInput1 = convertInputFileToArray();
        //Assume the array must be sorted first because we use binary search.
        //Sorting Input
        long totalMilisSorting1 = System.currentTimeMillis();
        int[] sortedInput1 = Sorter.quickSort(sequenceInput1);
        totalMilisSorting1 = System.currentTimeMillis() - totalMilisSorting1;
        System.out.println("Sorting Complete in " + totalMilisSorting1 + " milisecond");

        //Searching Input After Sorting
        long totalMilisSearchAfterSort1 = System.currentTimeMillis();
        int searchingResultAfterSort1 = Finder.fastSearch(sequenceInput1, 6112);
        totalMilisSearchAfterSort1 = System.currentTimeMillis() - totalMilisSearchAfterSort1;
        System.out.println("Searching Complete in " + totalMilisSearchAfterSort1 + " milisecond");

        int[] sequenceInput = convertInputFileToArray();

        System.out.println("//Using slow search and sorting");
        //Searching Input Before Sorting
        long totalMilisSearchBeforeSort = System.currentTimeMillis();
        int searchingResultBeforeSort = Finder.slowSearch(sequenceInput, 6112);
        totalMilisSearchBeforeSort = System.currentTimeMillis() - totalMilisSearchBeforeSort;
        System.out.println("Searching Complete in " + totalMilisSearchBeforeSort + " milisecond");

        //Sorting Input
        long totalMilisSorting = System.currentTimeMillis();
        int[] sortedInput = Sorter.slowSort(sequenceInput);
        totalMilisSorting = System.currentTimeMillis() - totalMilisSorting;
        System.out.println("Sorting Complete in " + totalMilisSorting + " milisecond");

        //Searching Input After Sorting
        long totalMilisSearchAfterSort = System.currentTimeMillis();
        int searchingResultAfterSort = Finder.slowSearch(sequenceInput, 6112);
        totalMilisSearchAfterSort = System.currentTimeMillis() - totalMilisSearchAfterSort;
        System.out.println("Searching Complete in " + totalMilisSearchAfterSort + " milisecond");

    }

    /**
     * Converting a file input into an array of integer.
     * @return an array of integer that represent an integer sequence.
     * @throws IOException in the case of the file is not found because of the wrong path of file.
     */
    public static int[] convertInputFileToArray() throws IOException {
        File sortingProblemFile = new File(pathFile);
        FileReader fileReader = new FileReader(sortingProblemFile);
        int[] sequenceInput = new int[numberOfItemToBeSorted];

        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String currentLine;
        int indexOfSequence = 0;
        while ((currentLine = bufferedReader.readLine()) != null) {
            sequenceInput[indexOfSequence] = Integer.parseInt(currentLine);
            indexOfSequence++;
        }
        return sequenceInput;
    }
}
