package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        employeesList.add(employees);
    }

    public double getNetSalaries() {
        double ret = 0.0;
        Iterator iterator =  employeesList.iterator();
        while (iterator.hasNext()) {
            Employees employee = (Employees)iterator.next();
            ret += employee.getSalary();
        }
        return ret;
    }

    public List<Employees> getAllEmployees() {
        List<Employees> allEmployees = new ArrayList<Employees>();
        Iterator iterator =  employeesList.iterator();
        while (iterator.hasNext()) {
            Employees employee = (Employees)iterator.next();
            allEmployees.add(employee);
        }
        return allEmployees;
    }
}
