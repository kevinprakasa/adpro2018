package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class RottenVeggies implements Veggies {

    public String toString() {
        return "RottenVeggies";
    }
}
